
from random import randint

def user():
    ''' R0 faire deviner un nombre à l'utilisateur'''
    # R1 Choisir un nombre
    solution = randint (1,999)
    print ("j'ai choisi un nombre entre 1 et 999.")
    # R1 Faire deviner ce nombre
    nb_essai = 0
    trouve = False
    while not trouve :
    
        # R2 Incrémenter le nombre d'essai
        nb_essai +=1

        # R2 Demander une proposition à l'utilisateur
        proposition = int(input(f'Proposition {nb_essai} : '))

        # R2 Donner un indice
        if proposition > solution:
            print ("Trop grand !")
        elif proposition < solution:
            print ("Trop petit !")
        else :
            print ("Bravo ! ")
            trouve = True

    # R1 Afficher le résultat
    print(f"Vous avez trouvé en {nb_essai} essais.")


def machine():
    ''' R0 deviner un nombre choisi par l'utilisateur'''
    # R1 Demander à l'utilisateur de choisir un nombre
    decision = 'n'
    while decision == 'n':
        decision = input('Avez-vous choisi un nombre compris entre 1 et 999 (o/n) ? ')
        if decision == 'n':
            print ("J'attends...")
        elif decision != 'o': #Instructions si entrée non valide
            print ("Je n'ai pas compris votre réponse")
            decision = 'n'



    # R1 Deviner ce nombre
    nb_essai = 0
    trouve = False
    limite_basse = 1
    limite_haute = 999
    while not trouve :

        # R2 Incrementer le nombre d'essais
        nb_essai +=1

        # R2 Choisir un nombre
        proposition = limite_basse + (limite_haute-limite_basse)// 2
        
        print(f'Proposition {nb_essai} : {proposition} ?')
        
        # R2 Recevoir un indice
        indice = input('Trop (g)rand, trop (p)etit ou (t)rouvé ? ')

        # R2 Definir les nouvelles limites
        if indice == "g" or indice == "G":
            limite_haute = proposition
        elif indice == "p" or indice == "P":
            limite_basse = proposition
        elif indice == "t" or indice == "T":
            trouve = True
        else :      #Instructions si entrée non valide
            nb_essai -= 1
            print (
            "Je n'ai pas compris la réponse. Merci de répondre \n"
            "g si ma proposition est trop grande \n"
            "p si ma proposition est trop petite \n"
            "t si j'ai trouvé le nombre")
            

    # R1 Afficher le résultat
    print (f"J'ai trouvé en {nb_essai} essais.")




def main ():
    jouer = True

    while jouer:
        decision = input ("1- L'ordinateur choisit un nombre et vous le devinez\n"
        "2- Vous choissez un nombre et l'ordinateur le devine\n"
        "0- Quitter le programme\n"
        "Votre choix : ")

        if decision == '1' :
            user ()
        elif decision == '2' :
            machine ()
        elif decision == '0' :
            jouer = False
            print ("Au revoir...")
        else :
            print ("je n'ai pas compris votre choix")

main ()